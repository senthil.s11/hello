@echo off 

SET PATH=%PATH%;C:\Program Files\Java\jdk1.8.0_112\bin

cd src\ssasubscribe

javac -classpath '.;./jars/commons-codec-1.6.jar;./jars/jetty-continuation-9.4.3.v20170317.jar;./jars/jetty-http-9.4.3.v20170317.jar;./jars/jetty-io-9.4.3.v20170317.jar;./jars/jetty-security-9.4.3.v20170317.jar;./jars/jetty-server-9.4.3.v20170317.jar;./jars/jetty-servlet-9.4.3.v20170317.jar;./jars/jetty-servlets-9.4.3.v20170317.jar;./jars/jetty-util-9.4.3.v20170317.jar;./jars/log4j-1.2.17.jar;./jars/servlet-api-3.1.jar;./jars/slf4j-api-1.7.5.jar;./jars/slf4j-log4j12-1.7.5.jar' *.java

mkdir com\att\datarouter\pubsub\ssasubscribe

copy *.class com\att\datarouter\pubsub\ssasubscribe

java -cp .;./jars/commons-codec-1.6.jar;./jars/jetty-continuation-9.4.3.v20170317.jar;./jars/jetty-http-9.4.3.v20170317.jar;./jars/jetty-io-9.4.3.v20170317.jar;./jars/jetty-security-9.4.3.v20170317.jar;./jars/jetty-server-9.4.3.v20170317.jar;./jars/jetty-servlet-9.4.3.v20170317.jar;./jars/jetty-servlets-9.4.3.v20170317.jar;./jars/jetty-util-9.4.3.v20170317.jar;./jars/log4j-1.2.17.jar;./jars/servlet-api-3.1.jar;./jars/slf4j-api-1.7.5.jar;./jars/slf4j-log4j12-1.7.5.jar com.att.datarouter.pubsub.ssasubscribe.SSASubscriber