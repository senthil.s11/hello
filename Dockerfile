from openjdk:latest
Copy ./  ./datarouter
Workdir ./datarouter
CMD . ./env 
CMD rm -rf demosimplesub \
    && mkdir -p demosimplesub
    #&& mkdir -p demosimplesub/certs
Copy ./certs  ./demosimplesub/certs
COPy  ./product/ssasubscribe.jar ./demosimplesub
Copy  ./src/ssasubscribe/jars/*.jar ./demosimplesub/
Copy  ./bin/com ./demosimplesub/com
CMD cat "!eof'\n"\
"log4j.debug=TRUE \n"\
"log4j.rootLogger=INFO,Root,debug \n"\
"log4j.appender.Root=org.apache.log4j.DailyRollingFileAppender \n"\
"log4j.appender.Root.file=subscriber.log \n "\
"log4j.appender.Root.datePattern='.'yyyyMMdd \n"\
"log4j.appender.Root.append=true \n"\
"log4j.appender.Root.layout=org.apache.log4j.PatternLayout \n"\
"log4j.appender.Root.layout.ConversionPattern=%d %p %t %m%n\n " \
"!eof \n" \ >./demosimplesub/log4j.properties
CMD  cd demosimplesub\
    && export CLASSPATH=$(echo . *.jar | tr ' ' ':') \
    && java  com.att.datarouter.pubsub.ssasubscribe.SSASubscriber 
