. ./env
rm -rf demoftpfinder
mkdir -p demoftpfinder
cp product/*.jar src/queue/jars/*.jar demoftpfinder
cat <<'!eof' >demoftpfinder/log4j.properties
log4j.debug=FALSE
log4j.rootLogger=INFO,Root

log4j.appender.Root=org.apache.log4j.DailyRollingFileAppender
log4j.appender.Root.file=subscriber.log
log4j.appender.Root.datePattern='.'yyyyMMdd
log4j.appender.Root.append=true
log4j.appender.Root.layout=org.apache.log4j.PatternLayout
log4j.appender.Root.layout.ConversionPattern=%d %p %t %m%n
!eof
cat <<!eof >demoftpfinder/configfile.properties
#QueueDirectories: queues
Processors: publish
publish.Class: com.att.datarouter.pubsub.filedeposit.FileDepositor
#publish.Threads: 1
#publish.OutputDirectory: output
Factories: scanner
scanner.Class: com.att.datarouter.pubsub.remote.FtpFileScanner
#scanner.Destination: publish
scanner.Directory: $FTPDIR
scanner.Host: $FTPHOST
#scanner.ScanIntervalSeconds: 10
#scanner.FailureIntervalSeconds: 600
!eof
cd demoftpfinder
CLASSPATH=$(echo . *.jar | tr ' ' ':') java com.att.datarouter.pubsub.queue.Main
