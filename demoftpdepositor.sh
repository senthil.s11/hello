#!/bin/bash
. ./env
rm -rf demoftpdeposit
mkdir -p demoftpdeposit
mkdir -p demoftpdeposit/data
cp product/*.jar src/queue/jars/*.jar demoftpdeposit
cat <<'!eof' >demoftpdeposit/log4j.properties
log4j.debug=FALSE
log4j.rootLogger=INFO,Root

log4j.appender.Root=org.apache.log4j.DailyRollingFileAppender
log4j.appender.Root.file=subscriber.log
log4j.appender.Root.datePattern='.'yyyyMMdd
log4j.appender.Root.append=true
log4j.appender.Root.layout=org.apache.log4j.PatternLayout
log4j.appender.Root.layout.ConversionPattern=%d %p %t %m%n
!eof
cat <<!eof >demoftpdeposit/configfile.properties
#QueueDirectories: queues
Processors: publish
publish.Class: com.att.datarouter.pubsub.remote.FtpDepositor
#publish.Threads: 1
publish.Directory: $FTPDIR
publish.Host: $FTPHOST
Factories: scanner
scanner.Class: com.att.datarouter.pubsub.filescan.FileScanner
#scanner.Destination: publish
#scanner.Directory: data
#scanner.MinAgeSeconds: 10
#scanner.ScanIntervalSeconds: 10
scanner.DeleteOrGZip: gzip
#scanner.GZipRetentionDays: 30
!eof
cd demoftpdeposit
CLASSPATH=$(echo . *.jar | tr ' ' ':') java com.att.datarouter.pubsub.queue.Main
